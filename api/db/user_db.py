from pymongo import MongoClient
from passlib.context import CryptContext
from ..models.user import UserInDB, UserCreate

client = MongoClient("mongodb://your_mongodb_connection_string")
db = client["your_database_name"]
users_collection = db["users"]

pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")


def hash_password(password: str) -> str:
    return pwd_context.hash(password)


async def add_user(user: UserCreate) -> UserInDB:
    user_data = user.dict()
    user_data["hashed_password"] = hash_password(user.password)

    result = await users_collection.insert_one(user_data)
    created_user = UserInDB(**user_data, _id=result.inserted_id)
    return created_user


async def get_user_by_username(username: str) -> UserInDB:
    user_data = await users_collection.find_one({"username": username})
    if user_data:
        return UserInDB(**user_data)
    return None


def verify_password(plain_password: str, hashed_password: str) -> bool:
    return pwd_context.verify(plain_password, hashed_password)
