from fastapi import APIRouter, Depends, HTTPException
from fastapi.security import OAuth2PasswordRequestForm
from . import models
from api.db import (
    add_user,
    get_user_by_username,
    verify_password,
    hash_password,
)

router = APIRouter()


@router.post("/signup", response_model=models.UserInDB)
async def signup(user: models.UserCreate):
    existing_user = await get_user_by_username(user.username)
    if existing_user:
        raise HTTPException(status_code=400, detail="Username already taken")

    hashed_password = hash_password(user.password)
    user_in_db = models.UserInDB(
        **user.dict(), hashed_password=hashed_password
    )
    created_user = await add_user(user_in_db)
    return created_user


@router.post("/login")
async def login(form_data: OAuth2PasswordRequestForm = Depends()):
    user = await get_user_by_username(form_data.username)
    if not user or not verify_password(
        form_data.password, user["hashed_password"]
    ):
        raise HTTPException(status_code=401, detail="Invalid credentials")
