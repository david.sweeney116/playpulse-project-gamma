import unittest
from fastapi import FastAPI
from fastapi.testclient import TestClient
import requests

app = FastAPI()

STEAM_API_KEY = "8CF9FA3D0E4142DCB074C5352C9FF7DF"


@app.get("/")
def read_root():
    return {"message": "Welcome to the Steam App List API"}


class TestApp0(unittest.TestCase):
    def setUp(self):
        self.client = TestClient(app)

    def test_read_root(self):
        response = self.client.get("/")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            response.json(), {"message": "Welcome to the Steam App List API"}
        )


if __name__ == "__main__":
    unittest.main()


@app.get("/get_app_list")
def get_steam_app_list(search_query: str = None, sort_order: str = "asc"):
    url = "https://api.steampowered.com/ISteamApps/GetAppList/v2/"
    response = requests.get(url)
    if response.status_code == 200:
        data = response.json()
        apps = data["applist"]["apps"]
        apps = [app for app in apps if app["name"].strip() != ""]

        def sorting_key(app):
            app_name = app["name"]
            index = next(
                (i for i, char in enumerate(app_name) if char.isalnum()),
                len(app_name),
            )
            key = app_name[index:]
            return key if sort_order == "asc" else key[::-1]

        apps.sort(key=sorting_key)
        app_dict = {app["appid"]: app["name"] for app in apps}

        if search_query:
            filtered_apps = {
                appid: app_name
                for appid, app_name in app_dict.items()
                if search_query.lower() in app_name.lower()
            }
        else:
            filtered_apps = app_dict

        app_list = [
            {"app_id": appid, "app_name": app_name}
            for appid, app_name in filtered_apps.items()
        ]
        return {"app_list": app_list}
    else:
        return {
            "error": f"Failed to retrieve data. Status code: {response.status_code}"
        }


class TestApp1(unittest.TestCase):
    def setUp(self):
        self.client = TestClient(app)

    def test_get_app_list(self):
        response = self.client.get("/get_app_list")
        self.assertEqual(response.status_code, 200)
        response_json = response.json()
        self.assertTrue("app_list" in response_json)
        self.assertTrue(isinstance(response_json["app_list"], list))
        self.assertTrue(
            all(
                "app_id" in app and "app_name" in app
                for app in response_json["app_list"]
            )
        )
        self.assertGreaterEqual(len(response_json["app_list"]), 10)
        response = self.client.get("/get_app_list?search_query=game")
        response_json = response.json()
        self.assertTrue(
            all(
                "game" in app["app_name"].lower()
                for app in response_json["app_list"]
            )
        )
        response = self.client.get("/get_app_list?sort_order=desc")
        response_json = response.json()


if __name__ == "__main__":
    unittest.main()


@app.get("/get_app_details/{app_id}")
def get_steam_app_details(app_id: int):
    url = f"https://store.steampowered.com/api/appdetails?appids={app_id}&key={STEAM_API_KEY}"
    response = requests.get(url)
    if response.status_code == 200:
        data = response.json()
        app_details = data.get(str(app_id), {})
        return {"app_details": app_details}
    else:
        return {
            "error": f"Failed to retrieve app details. Status code: {response.status_code}"
        }


class TestApp2(unittest.TestCase):
    def setUp(self):
        self.client = TestClient(app)

    def test_get_app_details(self):
        app_id = "4000"
        response = self.client.get(f"/get_app_details/{app_id}")
        self.assertEqual(response.status_code, 200)
        self.assertIn("app_details", response.json())
        app_details = response.json()["app_details"]
        self.assertIsInstance(app_details, dict)


if __name__ == "__main__":
    unittest.main()
