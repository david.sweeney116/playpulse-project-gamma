import { AuthProvider } from "@galvanize-inc/jwtdown-for-react";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import SignupForm from "./SignupForm";
import LoginForm from "./LoginForm";
import Nav from "./Nav";
import Main from "./Main";
import AppDetails from "./AppDetails";

function App() {
  return (
    <div className="container">
      <BrowserRouter>
        <AuthProvider baseUrl={process.env.REACT_APP_API_HOST}>
          <Nav />
          <Routes>
            <Route exact path="/" element={<Main />}></Route>
            <Route exact path="/token" element={<LoginForm />}></Route>
            <Route exact path="/accounts" element={<SignupForm />}></Route>
            <Route path="/app/:appId" element={<AppDetails />} />
          </Routes>
        </AuthProvider>
      </BrowserRouter>
    </div>
  );
}

export default App;
