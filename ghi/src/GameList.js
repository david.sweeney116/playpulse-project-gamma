import React, { useState, useEffect } from "react";

function GameList() {
  const [games, setGames] = useState([]);

  useEffect(() => {
    fetch("http://localhost:8000/get_app_list/")
      .then((response) => response.json())
      .then((data) => setGames(data))
      .catch((error) => console.error(error));
  }, []);

  return (
    <div>
      <h2>List of Games</h2>
      <ul>
        {games.map((game) => (
          <li key={game.id}>{game.name}</li>
        ))}
      </ul>
    </div>
  );
}

export default GameList;
