import { NavLink } from "react-router-dom";
import useToken from "@galvanize-inc/jwtdown-for-react";

const Nav = () => {
  const { logout } = useToken();
  const { token } = useToken();

  const handleLogout = async () => {
    try {
      await logout();
    } catch (error) {
      console.error("Logout failed:", error);
    }
  };

  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <NavLink className="navbar-brand" to="/">
              <img
                id="imgp"
                src="https://cdn.discordapp.com/attachments/684190605475315865/1164284222962405526/b26969b68c9807edc9f60de056ab5085.png?ex=6542a72e&is=6530322e&hm=fa8525061b262aca79aa1a1e34d2d38bcf322eddbf2e911200d6037c2fc1dd0f&"
              ></img>
            </NavLink>
            <div id="divr">
              <li className="nav-item">
                <NavLink className="nav-link" aria-current="page" to="/token">
                  Login
                </NavLink>
              </li>
              <li className="nav-item">
                <NavLink
                  className="nav-link"
                  aria-current="page"
                  to="/accounts"
                >
                  Sign Up
                </NavLink>
              </li>
              {token ? (
                <li id="logoutdiv">
                  <button id="logoutdiv" onClick={handleLogout}>
                    Logout
                  </button>
                </li>
              ) : null}
            </div>
          </ul>
        </div>
      </div>
    </nav>
  );
};

export default Nav;
