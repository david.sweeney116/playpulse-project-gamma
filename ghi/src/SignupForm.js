import { useState } from "react";
import useToken from "@galvanize-inc/jwtdown-for-react";
import { useNavigate } from "react-router-dom";
import "./index.css";

const SignupForm = () => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [first_name, setFirst] = useState("");
  const [last_name, setLast] = useState("");
  const { register } = useToken();
  const navigate = useNavigate();

  const handleRegistration = (e) => {
    e.preventDefault();
    const accountData = {
      username: username,
      password: password,
      first_name: first_name,
      last_name: last_name,
    };
    register(accountData, `${process.env.REACT_APP_API_HOST}/api/accounts`);
    e.target.reset();
    navigate("/");
  };

  return (
    <div id="centerlogs" className="card text-bg-light mb-3">
      <h5 id="logsize" className="card-header">
        Signup
      </h5>
      <div className="card-body">
        <form onSubmit={(e) => handleRegistration(e)}>
          <div id="logdiv" className="mb-3">
            <label id="loglabl" className="form-label">
              Username:
            </label>
            <input
              name="username"
              type="text"
              className="form-control"
              onChange={(e) => {
                setUsername(e.target.value);
              }}
            />
          </div>
          <div id="logdiv" className="mb-3">
            <label id="loglabl" className="form-label">
              Password:
            </label>
            <input
              name="password"
              type="password"
              className="form-control"
              onChange={(e) => {
                setPassword(e.target.value);
              }}
            />
          </div>
          <div id="logdiv" className="mb-3">
            <label id="loglabl" className="form-label">
              First Name:
            </label>
            <input
              name="first_name"
              type="text"
              className="form-control"
              onChange={(e) => {
                setFirst(e.target.value);
              }}
            />
          </div>
          <div id="logdiv" className="mb-3">
            <label id="loglabl" className="form-label">
              Last Name:
            </label>
            <input
              name="last_name"
              type="text"
              className="form-control"
              onChange={(e) => {
                setLast(e.target.value);
              }}
            />
          </div>
          <div>
            <input className="btn btn-primary" type="submit" value="Register" />
          </div>
        </form>
      </div>
    </div>
  );
};

export default SignupForm;
