const apiUrl = "http://localhost:8000/get_app_list";

function fetchSteamAppList() {
  fetch(apiUrl)
    .then((response) => {
      if (!response.ok) {
        throw new Error(`Failed to fetch data: ${response.status}`);
      }
      return response.json();
    })
    .then((data) => {
      const appList = data.app_list;

      if (appList.length === 0) {
        console.log("No Steam apps found.");
      } else {
        console.log("Steam App List:");
        appList.forEach((app) => {
          console.log(`App ID: ${app.app_id}, App Name: ${app.app_name}`);
        });
      }
    })
    .catch((error) => {
      console.error("Error:", error);
    });
}

fetchSteamAppList();
