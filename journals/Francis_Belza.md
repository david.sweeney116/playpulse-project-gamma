Francis Belza "PlayPulse" Project Gamma Journal

9/26/2023
Today I spent more time trying to understand the authentication process. So far I've just made the necessary changes in the docker-compose file and created the authentication.py file.

9/27/2023
Finally got the authenticator to work. We can now create a user, log in and log out. I will continue working on authorization for specific views.

10/09/2023
First day back after Autumn Break. Had a morning standup with the team and decided to scale back even more due to one of our members departing due to receiving a job offer. Today I will be working on the frotn end authentication (sign up and login forms)

10/10/2023
Had another morning standup with the team so everyone is informed on what everyone is working on. Both Signup and Login forms are functioning after some hurdles. Tomorrow we plan on building the React component to display our third party video game data.

10/11/2023
Had to adjust the game plan for today. Instead of starting the react component for game data, I will instead work on implementing logout functionality. There was also an issue we ran into when trying to access mongo express with our defined credentials in our .yaml file. Dalonte is looking into this.
@5:10 PM I pushed my work into the Testing branch.

10/16/2023
Out sick

10/17/2023
Working on authorization for the home page and game data. Users should be logged in in order to view main list of games.

10/23/2023
Took some time away from the project over the weekend to study for practice assessment. Our project is basically finished but we need to complete CI/CD as well as unit tests.

10/26/2023
Still working on unit test for account creation. There is a roadblock when I try to run the unit test as it says my imported modules are not found (fastapi.testclient). Will reach out to Dalonte for guidance. Once unit tests are passing, we will start CI/CD. README will also be worked on today/tomorrow.

10/27/2023
Switched my unit test to test for getting a token with a valid user. Test passes.
