Kai Martin "PlayPulse" Project Gamma Journal

9/25:
Added MongoDB functionality to our project and created the readme files for journaling

10/13:
Added numerous quality updates to games list functionality including:
    * Created search functionality
        - Updated search functionality to fix error where when you inputted a space the search came up with no results
        - Updated search functionality to go back to first page when searching so that searches don't come up blank unless there really is no game
    * Increased game list to 20
    * Sorted list into alphabetical order
        - Filtered list to start with the first character of the game so that games with spaces or special characters at the beginning of their name will skip to just the game name

10/16 & 10/17;
Created a game detail page that links games from the main game list page to a seperate page that shows the game and its details

10/18+:
Working to create a sorting function for the main game list page so that it can sort in alphabetical order, newest and oldest games

Working to create a unit test

Working to update gitlab tasks
